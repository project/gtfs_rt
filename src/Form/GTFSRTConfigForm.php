<?php

namespace Drupal\gtfs_rt\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\gtfs\Entity\Agency;

/**
 *
 */
class GTFSRTConfigForm extends ConfigFormBase {
  const SETTINGS = 'gtfs_rt.settings';

  /**
   *
   */
  public function getFormId() {
    return 'gtfs_rt_settings';
  }

  /**
   *
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   *
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $agencies = Agency::loadMultiple();

    $form['agency_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Agency'),
      '#description' => $this->t('The agency for which the GTFS RT feed provides information'),
      '#options' => ['' => '- none -'] + array_map(function (Agency $agency) {
        return sprintf("%s (Feed %s)", $agency->get('agency_name')->value, $agency->get('feed_reference')->target_id);
      }, $agencies),
      '#default_value' => $config->get('agency_id')
    ];

    $form['feed_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('GTFS RT Feed URL'),
      '#description' => $this->t('The base of the GTFS RT feed, wthout the command parameter'),
      '#default_value' => $config->get('feed_url'),
    ];

    $form['refresh_interval'] = [
      '#type' => 'number',
      '#title' => $this->t('Refresh Interval'),
      '#default_value' => $config->get('refresh_interval', 30),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   *
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('agency_id', $form_state->getValue('agency_id'))
      ->set('refresh_interval', $form_state->getValue('refresh_interval'))
      ->set('feed_url', $form_state->getValue('feed_url'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
