<?php

namespace Drupal\gtfs_rt\Services;

use Drupal;
use Drupal\gtfs\Entity\Agency;
use Drupal\gtfs\Entity\Route;
use Drupal\gtfs_rt\Form\GTFSRTConfigForm;

class GTFSRTFetcherUpdates implements GTFSRTFetcherInterface {

  public static function getCurrentData() {
    $feed_url = Drupal::config(GTFSRTConfigForm::SETTINGS)->get('feed_url');
    $feed_url = sprintf("%s/tripUpdates?format=json", $feed_url);
    return json_decode(file_get_contents($feed_url));
  }

  public function fetch() {
    @ignore_user_abort(TRUE);
    $refresh_interval = (int) Drupal::config(GTFSRTConfigForm::SETTINGS)->get('refresh_interval', 30);
    if (!Drupal::lock()->acquire('gtfs_rt_fetch_updates', $refresh_interval)) {
      return;
    }
    $table = 'gtfs_rt_trip_updates';
    $db = Drupal::database();
    $agency_id = Drupal::config(GTFSRTConfigForm::SETTINGS)->get('agency_id');
    $agency = Agency::load($agency_id);
    $agency_id = $agency->get('agency_id')->value;
    $insert_query = $db->insert("{$table}_temp")->fields([
      'agency_id',
      'stop_id',
      'route_id',
      'vehicle',
      'arrival',
      'destination',
    ]);
    $tripUpdates = self::getCurrentData();
    foreach ($tripUpdates->entity as $entity) {
      $gtfs_route_id = $entity->tripUpdate->trip->routeId;
      $route = Route::getById($agency, $gtfs_route_id);
      // TODO
      //  Get the destination that's the opposite direction
      //  (maybe inaccurate implementation)
      $destination = $db->query(
        "SELECT `trip_headsign`
         FROM {gtfs_trip_field_data}
         WHERE `direction_id` != :direction
         AND `route_id` = :route_id
         LIMIT 1",
        [
          ':direction' => $entity->tripUpdate->trip->directionId,
          ':route_id' => $route->id(),
        ]
      )->fetchColumn();
      foreach ($entity->tripUpdate->stopTimeUpdate as $update) {
        if (isset($update->arrival)) {
          $insert_query->values([
            'agency_id' => $agency_id,
            'stop_id' => $update->stopId,
            'route_id' => $gtfs_route_id,
            'vehicle' => $entity->tripUpdate->vehicle->id,
            'arrival' => (int) $update->arrival->time,
            'destination' => $destination,
          ]);
        }
      }
    }
    $db->truncate("{$table}_temp")->execute();
    $insert_query->execute();
    $db->truncate($table)->execute();
    $db->insert($table)->from($db->select("{$table}_temp", 't')->fields('t'))->execute();
    Drupal::lock()->release('gtfs_rt_fetch_updates');
  }
}
