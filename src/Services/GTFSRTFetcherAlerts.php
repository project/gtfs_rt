<?php

namespace Drupal\gtfs_rt\Services;

use Drupal\gtfs_rt\Form\GTFSRTConfigForm;

class GTFSRTFetcherAlerts implements GTFSRTFetcherInterface {
  public function fetch() {
    // Empty implementation; we don't have a GTFS RT alerts feed
  }
}