<?php

namespace Drupal\gtfs_rt\Services;

interface GTFSRTFetcherInterface {
  public function fetch();
}