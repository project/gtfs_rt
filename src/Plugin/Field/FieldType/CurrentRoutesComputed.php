<?php

namespace Drupal\gtfs_rt\Plugin\Field\FieldType;

use Drupal;
use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;
use PDO;

/**
 * Represents a configurable entity path field.
 */
class CurrentRoutesComputed extends FieldItemList {

  use ComputedItemListTrait;

  /**
   * {@inheritdoc}
   */
  protected function computeValue() {

    $entity = $this->getEntity();

    $route_ids = Drupal::database()->query(
      'SELECT DISTINCT `route_id`
      FROM `gtfs_rt_trip_updates`
      WHERE `stop_id` = :stop_id',
      [':stop_id' => $entity->get('stop_id')->value]
    )->fetchAll(PDO::FETCH_COLUMN);

    if (count($route_ids)) {
      $routes = Drupal::database()
        ->select('gtfs_route_field_data', 'fd')
        ->condition('fd.route_id', $route_ids, 'IN')
        ->fields('fd', ['id'])
        ->addTag('gtfs_route_access')
        ->execute()
        ->fetchAll(PDO::FETCH_COLUMN);

      $this->setValue($routes);
    }


  }

}
