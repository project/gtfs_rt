<?php

namespace Drupal\gtfs_rt\Plugin\rest\resource;

use Drupal;
use Drupal\rest\ResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\gtfs_rt\Form\GTFSRTConfigForm;
use Drupal\Core\Cache\CacheableMetadata;
use PDO;

// use Drupal\gtfs\Entity\Agency;
// use Drupal\gtfs\Entity\AgencyResource;
// use Drupal\gtfs\Entity\Route;
// use Drupal\gtfs\Entity\RouteResource;
// use Drupal\gtfs\Entity\Stop;
// use Drupal\gtfs\Entity\StopResource;

/**
 * Provides GTFS RT alerts are a REST resource.
 *
 * @RestResource(
 *   id = "gtfs_route_stop_alerts_resource",
 *   label = @Translation("GTFS RT alerts"),
 *   uri_paths = {
 *     "canonical" = "/gtfs/api/v1/agencies/{agency_id}/routes/{route_id}/stops/{stop_id}/alerts"
 *   }
 * )
 */
class RouteStopAlertsResource extends ResourceBase {

  public static $url = '/gtfs/api/v1/agencies/{agency_id}/routes/{route_id}/stops/{stop_id}/predictions';

  /**
   * Responds to stop GET requests.
   *
   * @return \Drupal\gtfs\Plugin\rest\resource\ResourceResponse
   */
  public function get($agency_id = NULL, $route_id = NULL, $stop_id = NULL) {

    Drupal::service('page_cache_kill_switch')->trigger();

    $alerts = self::alerts($agency_id, $route_id, $stop_id);

    $meta = GTFSRTResourceMeta::create($agency_id, $route_id, $stop_id);

    gtfs_rt_fetch_async();

    return new GTFSRTResourceResponse($alerts, $meta);
  }

  public static function alerts($agency_id = NULL, $route_id = NULL, $stop_id = NULL) {
    $alerts = Drupal::database()->query(
      "SELECT `text`
       FROM {gtfs_rt_alerts}
       WHERE `agency_id` = :agency_id
       AND `route_id` = :route_id
       AND (
           `stop_id` = :stop_id
            OR `stop_id` = 'all'
       )
       AND `start` < :now
       AND `end` > :now",
      [
        ':agency_id' => $agency_id,
        ':route_id' => $route_id,
        ':stop_id' => $stop_id,
        ':now' => time(),
      ]
    )->fetchAll(PDO::FETCH_COLUMN) ?: [];

    return $alerts;
  }

}
