<?php

namespace Drupal\gtfs_rt\Plugin\rest\resource;

use Drupal;

class GTFSRTResourceMeta {
  public static function create($agency_id, $route_id, $stop_id) {
    $base = Drupal::request()->getSchemeAndHttpHost();
    $last_fetch = Drupal::state()->get('gtfs_rt.last_fetch', 0);

    return [
      'last_fetch' => $last_fetch,
      'links' => [
        'agency' => "{$base}/gtfs/api/v1/agencies/{$agency_id}",
        'route' => "{$base}/gtfs/api/v1/agencies/{$agency_id}/routes/{$route_id}",
        'stop' => "{$base}/gtfs/api/v1/stops/{$stop_id}"
      ]
    ];
  }
}