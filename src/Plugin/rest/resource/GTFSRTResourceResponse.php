<?php

namespace Drupal\gtfs_rt\Plugin\rest\resource;

use Drupal;
use Drupal\rest\ResourceResponse;
use Drupal\gtfs_rt\Form\GTFSRTConfigForm;
use Drupal\Core\Cache\CacheableMetadata;

class GTFSRTResourceResponse extends ResourceResponse {
  public function __construct($data = NULL, $meta = [], $status = 200, $headers = []) {
    parent::__construct(['meta' => $meta, 'data' => $data], $status, $headers);
    $refresh_interval = (int) Drupal::config(GTFSRTConfigForm::SETTINGS)->get('refresh_interval', 30);
    $disable_cache = new CacheableMetadata();
    $disable_cache->setCacheMaxAge($refresh_interval / 2);
    $this->addCacheableDependency($disable_cache);
  }
}