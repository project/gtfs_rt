<?php

namespace Drupal\gtfs_rt\Plugin\rest\resource;

use Drupal;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\Core\Cache\CacheableMetadata;

/**
 * Fetches new RT data
 *
 * @RestResource(
 *   id = "gtfs_rt_fetch_resource",
 *   label = @Translation("Fetch GTFS RT data"),
 *   uri_paths = {
 *     "canonical" = "/gtfs/api/v1/rt/refresh"
 *   }
 * )
 */
class GTFSRTFetchResource extends ResourceBase {
  /**
   * Responds to GET requests.
   *
   * @return \Drupal\gtfs\Plugin\rest\resource\ResourceResponse
   */
  public function get() {
    @ignore_user_abort(TRUE);

    Drupal::service('page_cache_kill_switch')->trigger();

    $response['updates'] = Drupal::service('gtfs_rt.fetcher.updates')->fetch();

    $response['alerts'] = Drupal::service('gtfs_rt.fetcher.alerts')->fetch();

    $disable_cache = new CacheableMetadata();
    $disable_cache->setCacheMaxAge(0);

    return (new ResourceResponse($response))->addCacheableDependency($disable_cache)->addCacheableDependency($response);
  }

}
