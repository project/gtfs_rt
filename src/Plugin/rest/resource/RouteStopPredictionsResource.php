<?php

namespace Drupal\gtfs_rt\Plugin\rest\resource;

use Drupal;
use Drupal\gtfs\Entity\Agency;
use Drupal\gtfs\Entity\Route;
use Drupal\gtfs\Entity\Stop;
use Drupal\gtfs_stop_display\Entity\RouteStopOverride;
use Drupal\rest\Plugin\ResourceBase;
use PDO;


/**
 * Provides GTFS RT predictions as a rest resource.
 *
 * @RestResource(
 *   id = "gtfs_route_stop_predictions_resource",
 *   label = @Translation("GTFS RT predictions"),
 *   uri_paths = {
 *     "canonical" = "/gtfs/api/v1/agencies/{agency_id}/routes/{route_id}/stops/{stop_id}/predictions"
 *   }
 * )
 */
class RouteStopPredictionsResource extends ResourceBase {

  public static $url = '/gtfs/api/v1/agencies/{agency_id}/routes/{route_id}/stops/{stop_id}/predictions';

  /**
   * Responds to stop GET requests.
   *
   * @return \Drupal\gtfs_rt\Plugin\rest\resource\GTFSRTResourceResponse
   */
  public function get($agency_id = NULL, $route_id = NULL, $stop_id = NULL) {

    Drupal::service('page_cache_kill_switch')->trigger();

    $gettingDataForTemporaryStop = Drupal::request()->get('override', FALSE);

    $data = self::predictions($agency_id, $route_id, $stop_id, $gettingDataForTemporaryStop);

    $meta = GTFSRTResourceMeta::create($agency_id, $route_id, $stop_id);

    return new GTFSRTResourceResponse($data, $meta);

  }

  public static function predictions($agency_id = NULL, $route_id = NULL, $stop_id = NULL, $gettingDataForTemporaryStop = FALSE) {
    $data = [ 'destinations' => [] ];

    $agency = Agency::getById($agency_id);
    $overridden = RouteStopOverride::loadForRouteAndUsualStop(Route::getById($agency, $route_id)->id(), Stop::getById($stop_id)->id());

    if ($overridden && !$gettingDataForTemporaryStop) {
      return $data;
    }

    $placeholders = [
      ':agency_id' => $agency_id,
      ':route_id' => $route_id,
      ':stop_id' => $stop_id,
    ];

    $destinations = Drupal::database()->query(
      "SELECT DISTINCT `destination`
       FROM {gtfs_rt_trip_updates}
       WHERE `agency_id` = :agency_id
       AND `route_id` = :route_id
       AND `stop_id` = :stop_id",
      $placeholders
    )->fetchAll(PDO::FETCH_COLUMN);

    foreach ($destinations as $destination) {
      $data['destinations'][] = [
        'name' => $destination,
        'predictions' => Drupal::database()->query(
          "SELECT `arrival`, `vehicle`
           FROM {gtfs_rt_trip_updates}
           WHERE `agency_id` = :agency_id
           AND `route_id` = :route_id
           AND `stop_id` = :stop_id
           AND `destination` = '{$destination}'
           ORDER BY `arrival`",
          $placeholders
        )->fetchAll(PDO::FETCH_ASSOC),
      ];
    }

    gtfs_rt_fetch_async();

    return $data;
  }

}
